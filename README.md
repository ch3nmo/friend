```yaml
# 显示名称
name: 
# 跳转地址
link: 
# 你的头像
avatar: 
# 你的描述
descr: 
#------------------------------#
#       以下字段为选填字段       #

# 边框及鼠标悬停的背景颜色，允许设置渐变色
--primary-color: linear-gradient( 135deg, #FFF886 10%, #F072B6 100%)
# 边框大小
border-width: 0px
# 边框样式
border-style: solid
# 鼠标悬停头像旋转角度
--primary-rotate: 0deg
# 边框动画 参考 https://developer.mozilla.org/zh-CN/docs/Web/CSS/animation
# 内置动画：borderFlash（边框闪现）、link_custom1(跑马灯)、link_custom(主颜色呼吸灯)
animation: borderFlash 1s infinite alternate
# 头像动画 参考 https://developer.mozilla.org/zh-CN/docs/Web/CSS/animation
# 内置动画：auto_rotate_left（左旋转）、auto_rotate_right（右旋转）
img_animation: auto_rotate_right .5s linear infinite
# 风格 可选项 item和card
card_style: card
# 自定义网站截图（当样式为card时可以自定义网站截图，防止api接口宕掉无法显示图）
screenshot: 
```
